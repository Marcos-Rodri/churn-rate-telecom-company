# Churn Rate - Telecom Company

Telecom company - Churn Rate Analysis

In this project, we will work on a dataset from a Telecom company that offers the following services: telephone lines, internet, streaming, online safety & backup, technical suport, among others. This study's goal is to analyze the firm's churn rate, which is the percentage of clients that cancell or do not renew their contracts in comparison to the total number of customers. So, we will focus our efforts to identify what are the main problems that could be increasing the firm's churn rate, in order to retain more clients. Finally, it is relevant to state that it is usually less costly for a company to maintain its current customers than attracting new ones, which highlights the importance of identifying the source of cancellations through data analysis.

How are we going to achieve this?

- Using Python to clean the dataset and to organize it.

- Employ Plotly library to visualize the information and to conduct an exploratory analysis.


Questions:

- 1) What is the firm's churn rate?

- 2) What are the possible causes for this churn rate? How can we reduce it?


Data Source: https://www.kaggle.com/blastchar/telco-customer-churn
